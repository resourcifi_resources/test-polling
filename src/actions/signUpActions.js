import { auth, database } from '../Inc/firebase.js'
export const SIGNUPSUCCESS = 'SIGNUPSUCCESS';
export const SIGNUPREQUEST = 'SIGNUPREQUEST';
export const SIGNUPERROR = 'SIGNUPERROR';

export function handleSignUpSuccess(res) {
  return {
    type: SIGNUPSUCCESS,
    payload: res,
    message: res.message,
  };
}

export function handleSignUpRequest() {
  return {
    type: SIGNUPREQUEST,
    message: 'loading',
  };
}

// to handle error
export function handleSignupError(err) {
  return {
    type: SIGNUPERROR,
    payload: err,
    message: err.message,
  };
}

// sending post request of signup data i.e. email and password to backend
export function _signupAction(data) {
return (dispatch) => {
    let { email,password, name } = data
    dispatch(handleSignUpRequest());
    auth.createUserWithEmailAndPassword(email,password)
    .then((res) => {
        database.ref(`users/${res.user.uid}`).set({
          name:name,
          email:email,
      })
        return dispatch(handleSignUpSuccess(res));
    })
    .catch((err) => {
      return dispatch(handleSignupError(err))});
  };
}
