import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import Login from './Components/Accounts/Login.jsx';
import SignUp from './Components/Accounts/SignUp.jsx';
import Questions from './Components/Questions.jsx';
import Poll from './Components/Poll.jsx';
import Stats from './Components/Stats.jsx';
import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/css/custom.css';
import store from './store.js'
class App extends Component {
  render() {
    return (
      <Provider store={ store }>
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component ={Questions} />
            <Route exact path="/poll" component ={Poll} />
            <Route exact path="/stats" component ={Stats} />
            <Route exact path="/login" component ={Login} />
            <Route exact path="/signup" component ={SignUp} />
          </Switch>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
