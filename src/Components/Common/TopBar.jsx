import React , { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { logOutAction, loginAction, checkLoginAction } from '../../actions/loginActions.js';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    } from 'reactstrap';
import { Link } from 'react-router-dom';
import { AccountIcon } from 'mdi-react';

class TopBar extends Component {
    constructor(props){
        super(props);
        this.state = {
            isOpen:false,
            userEmail:''
        }
    }
    toggle = () =>{
        this.setState({ isOpen: !this.state.isOpen })
    }

    componentDidMount(){
        this.props.checkLogIn();
    }

    componentWillReceiveProps(props){
        if(props.States.loginReducers.isLoggedIn === false){ 
            this.props.history.push('/login');
        }
        if(props.States.loginReducers.user){
            this.setState({ userEmail : props.States.loginReducers.user.email })
        }
    }
    render(){
        let { isOpen, userEmail } = this.state;
        return(
            <div>
                <Navbar color="dark" dark expand="md">
                    <NavbarBrand href="/">Polling App</NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={isOpen} navbar>
                        <Nav className="ml-auto" navbar>
                        <NavItem >
                            <NavLink className="text-white" tag={Link} to="/">{ userEmail && (<AccountIcon />)} {userEmail}</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink tag={Link} to="/">Home</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink tag={Link} to="/poll">Poll</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink tag={Link} to="/stats">Stats</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink className="cursor-pointer" onClick={ ()=>{this.props.logOut()} }>LogOut</NavLink>
                        </NavItem>
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    const States = state;
    return { States };
  }

const mapDispatchToProps = (dispatch, props) => {
    return {
      logOut: () => {
        dispatch(logOutAction());
      },
      logIn: () => {
        dispatch(loginAction());
      },
      checkLogIn : () => {
          dispatch(checkLoginAction());
      }
    }
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TopBar));