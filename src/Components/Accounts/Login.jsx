import React ,{ Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import { withRouter, Link } from 'react-router-dom';
import { Alert, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { connect } from 'react-redux';
import { loginAction, checkLoginAction } from '../../actions/loginActions.js'
class Login extends Component {
    constructor(props){
        super(props);
        this.state = {
            email:'',
            password:'',
            error:'',
            stepCount:0,
            error:''
        }
    }
    componentDidMount(){
        this.props.checkLogin();
    }
    componentWillReceiveProps(props){
        if(props.loginStates.loginReducers.isLoggedIn === true){
            this.props.history.push('/');
        }else if(props.loginStates.loginReducers.isLoggedIn.message){
            this.setState({ error:props.loginStates.loginReducers.isLoggedIn.message  })
            setTimeout(()=>{
                this.setState({ error:'' })                
            },2000)
        }else if(props.loginStates.loginReducers.isLoggedIn==='loading'){
            console.log('Loading... ')
        }

    }
    onChangeHandler = (name,value)=>{
        this.setState({ [name] : value })
    }
    
    handleLogin = () => {
        let data = {
            email: this.state.email,
            password: this.state.password,
        }
        if( data.email==='' || data.password ===''){
            this.setState({ error :'All Fields are mandatory' });
            setTimeout(()=>{
                this.setState({error:''})
            },2000)
        }else{
            this.props._login(data);
        }
    }

    
    render() {
        let { email, password, error } = this.state;
        return(
            <Container>
                <Row>
                    {error && (
                        <Alert className="alert-abs" color="danger">
                            {error}
                        </Alert>
                    )}
                    <Col sm={12} md={{ size: 4, offset: 4 }} lg={{ size: 4, offset: 4 }} className="login-box">
                        <h3 className="text-center">Login</h3>
                        <Form>
                            <FormGroup>
                                <Label>Email</Label>
                                <Input type="email" name="email" placeholder="Enter Email" value ={ email } onChange={ (e) => { this.onChangeHandler( e.target.name, e.target.value ) } }/>
                            </FormGroup>
                            <FormGroup>
                                <Label>Password</Label>
                                <Input type="password" name="password" placeholder="Enter Password" value ={ password } onChange={ (e) => { this.onChangeHandler( e.target.name, e.target.value ) } }/>
                            </FormGroup>
                            <Button color="success" onClick={ ()=>{ this.handleLogin() } }>Login</Button>
                            <br />
                            <p className="m-0 text-muted text-center">Don't have an account ? <Link to="signup" >SignUp</Link></p>
                        </Form>                        
                    </Col>
                </Row>
            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    const loginStates = state;
    return { loginStates };
  }

const mapDispatchToProps = (dispatch, props) => {
    return {
      _login: (data) => {
        dispatch(loginAction(data));
      },
      checkLogin : () => {
          dispatch(checkLoginAction())
      }
    }
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));