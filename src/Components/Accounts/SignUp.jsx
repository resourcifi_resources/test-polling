import React ,{ Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import { Alert, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { _signupAction } from '../../actions/signUpActions.js';
class SignUp extends Component {
    constructor(props){
        super(props);
        this.state = {
            name:'',
            email:'',
            password:'',
            error:''
        }
    }

    onChangeHandler = (name,value)=>{
        this.setState({ [name] : value })
    }

    handleSignUp = () => {
        let { name,email,password } = this.state
        let data = {
            name: name,
            email: email,
            password: password
        }        
        this.props._signUp(data);
        // this.props.history.push('/')
    }

    componentWillReceiveProps(props){
        if(props.loginStates.loginReducers.signUp.user){
            this.props.history.push('/');
        }else if(props.loginStates.loginReducers.signUp.message){
            this.setState({ error:props.loginStates.loginReducers.signUp.message  })
            setTimeout(()=>{
                this.setState({ error:'' })                
            },2000)
        }else if(props.loginStates.loginReducers.signUp==='loading'){
            console.log('Loading')
        }
    }   

    render() {
        let { name,email,password, error } = this.state
        return(
            <Container>
                 { error && (
                        <Alert className="alert-abs" color="danger">
                            {error}
                        </Alert>
                    )}
                <Row>
                    <Col sm={12} md={{ size: 4, offset: 4 }} lg={{ size: 4, offset: 4 }} className="login-box">
                        <h3 className="text-center">SignUp</h3>
                        <Form>
                            <FormGroup> 
                                <Label>Name</Label>
                                <Input type="text" name="name" placeholder="Enter Your Name" value ={ name } onChange={ (e) => { this.onChangeHandler( e.target.name, e.target.value ) } } />
                            </FormGroup>
                            <FormGroup>
                                <Label>Email</Label>
                                <Input type="email" name="email" placeholder="Enter Email" value ={ email } onChange={ (e) => { this.onChangeHandler( e.target.name, e.target.value ) } }/>
                            </FormGroup>
                            <FormGroup>
                                <Label>Password</Label>
                                <Input type="password" name="password" placeholder="Enter Password" value ={ password } onChange={ (e) => { this.onChangeHandler( e.target.name, e.target.value ) } } />
                            </FormGroup>
                            <Button color="success" onClick={ ()=>{ this.handleSignUp() } }>Submit</Button>
                            <p className="text-muted text-center">Already have an account ? <Link to="login" >Log In</Link></p>
                        </Form>                        
                    </Col>
                </Row>
            </Container>
        )
    }
}


const mapStateToProps = (state) => {
    const loginStates = state;
    return { loginStates };
  }

const mapDispatchToProps = (dispatch, props) => {
    return {
      _signUp: (data) => {
        dispatch(_signupAction(data));
      }
    }
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SignUp));