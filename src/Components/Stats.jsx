import React ,{ Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Container, Row, Col, Card, CardBody, CardTitle, CardText, Button, CardHeader  } from 'reactstrap';
import TopBar from './Common/TopBar.jsx'

class Stats extends Component {
    constructor(props){
        super(props);
        this.state = {
            active:false
        }
    }

    render() {
        return(
            <div>
                <TopBar />
                <Container>
                    <Row className="mt-2">
                        <Col md={12} sm={12}>
                            <h2>Stats</h2>
                            <hr></hr>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

export default withRouter(Stats);