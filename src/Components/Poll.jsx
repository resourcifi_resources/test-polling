import React ,{ Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Alert, Container, Row, Col, Card, CardBody, CardTitle, CardText, Button, CardHeader, CardFooter, CardDeck } from 'reactstrap';
import TopBar from './Common/TopBar.jsx'
import { auth, database } from '../Inc/firebase.js';
import { map, forEach } from 'lodash';
import { PollIcon } from 'mdi-react';
class Poll extends Component {
    constructor(props){
        super(props);
        this.state = {
            allQuestions:[],
            rawData:[],
            questionData:{},
            uid:'',
            error:''
        }
    }

    componentDidMount(){
        this.setState({loading:true})
    }

    onClickHandler = (i,name,uid)=>{
        let userId = auth.currentUser.uid;
        let { rawData } = this.state;
        let userData = rawData[uid];
        userData.uid = uid;
        ++userData.question.allOptions[i].poll 
        if(!userData.usersPolled){
            userData.usersPolled=[]
        }
        userData.usersPolled.push(userId);
        this.setState({ questionData: userData })
        
    }

    onSubmitHandler = ()=>{
        let questionData = this.state.questionData;
        let data = new Object 
        data = {
            question : questionData.question,
        }
        data.question.usersPolled = questionData.usersPolled;
        database.ref(`users/${questionData.uid}`).update(data);
        this.setState({ error:'poll recorded'  })
        setTimeout(()=>{
            this.setState({ error:''  })
        },2000)
    }

    componentWillReceiveProps(props){
        if(props.allStates.loginReducers.isLoggedIn===true){
            var self = this;
            database.ref('/users/').on('value', function(snapshot) {
            let uid = auth.currentUser.uid
            let allQuestions = [];
            let data;
            forEach(snapshot.val(), (value, key) => {
                if(key !== uid) {
                    value.userId = key
                    allQuestions.push(value)
                }
            });
            self.setState({ allQuestions,rawData:snapshot.val(), uid, loading:false });
        });
        }
    }

    render() {
        let { allQuestions, uid, error, loading } = this.state;
        return(
            <div>
                <TopBar />
                {loading ? (
                    <div className="text-center m-auto">
                        <img src={require('../assets/images/spinner.gif')} />
                    </div>
                ) : (

                        <Container>
                            { error && (
                                <Alert className="alert-abs" color="success">
                                    {error}
                                </Alert>
                            )}
                            <Row className="mt-2">
                            <Col md={12} sm={12}>
                                <h2>Questions asked by Users</h2>
                                <p className="text-muted">You can poll for a question by selecting any one option ad then clicking poll.</p>
                                <hr></hr>
                            </Col>
                            <CardDeck>
                            { allQuestions !==null && map(allQuestions,(question, i)=>{
                                let polled= false
                                if(!question.question ){
                                    return;
                                }
                                if(question.question.usersPolled ){
                                    let check =question.question.usersPolled.find((userId)=>{
                                        return userId !== uid
                                    })
                                    if(check !== null){
                                        polled =true
                                    }
                                }
                                return(
                                    
                                        <Card className={`mw-300`} key={i}>
                                       
                                        <CardHeader>Questions</CardHeader>
                                            <CardBody className={ polled ? 'background-disabled': '' }> 
                                                    <CardBody>
                                                        <CardTitle>{question.question && `${question.question.title} ?`}</CardTitle>
                                                        
                                                            <ul className="list-style-none">    
                                                                {question.question && question.question.allOptions.map((option,oi)=>{
                                                                    return(
                                                                        <li key={ oi }>
                                                                            <input name="option" type="radio" onClick={ (e)=>{ this.onClickHandler(oi,e.target.name,question.userId) } } />
                                                                            { ` ${option.title}` }</li>
                                                                    )
                                                                })}
                                                            </ul>
                                                    </CardBody>
                                                </CardBody>
                                                <CardFooter className="text-right">
                                                {
                                                    polled ? (
                                                        <Button color="default" className="mr-1" disabled><PollIcon size={16} /> Poll</Button> 
                                                    ):(
                                                        <Button color="primary" className="mr-1" onClick={ () => { this.onSubmitHandler() } }><PollIcon size={16} /> Poll</Button> 
                                                    )
                                                }
                                                </CardFooter>
                                        </Card>
                                )
                            }) }
                            </CardDeck>
                            </Row>
                        </Container>
                )}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    const allStates = state;
    return { allStates };
  }

export default  withRouter(connect(mapStateToProps)(Poll));
